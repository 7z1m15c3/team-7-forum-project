/* eslint-disable max-len */
import {ref, push, get, query, equalTo, orderByChild, update, onValue, set} from 'firebase/database';
import {db} from '../config/firebase-config';

export const fromPostsDocument = (snapshot) => {
  const postsDocument = snapshot.val();
  return Object.keys(postsDocument).map((key) => {
    const post = postsDocument[key];

    return {
      ...post,
      id: key,
      createdOn: new Date(post.createdOn),
      likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
    };
  });
};

export const addPost = (title, content, tags, handle) => {
  return push(
      ref(db, 'posts'),
      {
        title,
        content,
        tags,
        author: handle,
        createdOn: Date.now(),
      },
  )
      .then((result) => {
        return getPostById(result.key);
      });
};

export const getPostById = (id) => {
  return get(ref(db, `posts/${id}`))
      .then((result) => {
        if (!result.exists()) {
          throw new Error(`Post with id ${id} does not exist!`);
        }

        const post = result.val();
        post.id = id;
        post.createdOn = new Date(post.createdOn);
        if (!post.likedBy) {
          post.likedBy = [];
        } else {
          post.likedBy = Object.keys(post.likedBy);
        }

        return post;
      });
};

export const getLikedPosts = (handle) => {
  return get(ref(db, `users/${handle}`))
      .then((snapshot) => {
        if (!snapshot.val()) {
          throw new Error(`User with handle @${handle} does not exist!`);
        }

        const user = snapshot.val();
        if (!user.likedPosts) return [];

        return Promise.all(Object.keys(user.likedPosts).map((key) => {
          return get(ref(db, `posts/${key}`))
              .then((snapshot) => {
                const post = snapshot.val();

                return {
                  ...post,
                  createdOn: new Date(post.createdOn),
                  id: key,
                  likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
                };
              });
        }));
      });
};

export const getPostsByAuthor = (handle) => {
  return get(query(ref(db, 'posts'), orderByChild('author'), equalTo(handle)))
      .then((snapshot) => {
        if (!snapshot.exists()) return [];

        return fromPostsDocument(snapshot);
      });
};

export const getLivePosts = (listen) => {
  return onValue(ref(db, 'posts'), listen);
};

export const getAllPosts = () => {
  return get(ref(db, 'posts'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromPostsDocument(snapshot);
      });
};

export const likePost = (handle, postId) => {
  const updateLikes = {};
  updateLikes[`/posts/${postId}/likedBy/${handle}`] = true;
  updateLikes[`/users/${handle}/likedPosts/${postId}`] = true;

  return update(ref(db), updateLikes);
};

export const dislikePost = (handle, postId) => {
  console.log('in service dislikePost'); console.log(handle);

  const updateLikes = {};
  updateLikes[`/posts/${postId}/likedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/likedPosts/${postId}`] = null;

  return update(ref(db), updateLikes);
};

export const deletePost = async (id) => {
  const post = await getPostById(id);
  const updateLikes = {};

  post.likedBy.forEach((handle) => {
    updateLikes[`/users/${handle}/likedPosts/${id}`] = null;
  });

  await update(ref(db), updateLikes);

  return update(ref(db), {
    [`/posts/${id}`]: null,
  });
};

export const updatePost = async (postId, title, content, tags, author) => {
  set(ref(db, 'posts/' + postId), {
    author: author,
    content: content,
    createdOn: Date.now(),
    postId: postId,
    tags: tags,
    title: title,
  });
};
