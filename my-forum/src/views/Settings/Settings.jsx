/* eslint-disable no-unused-vars */
/* eslint-disable no-mixed-operators */
/* eslint-disable array-callback-return */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import './Settings.css';
import {Avatar} from '@mui/material';
import {useContext, useEffect, useState} from 'react';
import AppContext from '../../data/app-state';
import {AiOutlineEdit, AiOutlineSave} from 'react-icons/ai';
import {
  updateUser,
  updateUserProfilePicture,
} from '../../services/users.service';
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from 'firebase/storage';
import {storage} from '../../config/firebase-config';
import {v4} from 'uuid';
import {GrUpload} from 'react-icons/gr';

export default function Settings() {
  const {user, userData, setContext} = useContext(AppContext);

  const [clickEditBtnName, setclickEditBtnName] = useState('false');
  const [clickEditBtnEmail, setclickEditBtnEmail] = useState('false');
  const [clickEditBtnPassword, setclickEditBtnPassword] = useState('false');
  const handle = userData.username;
  const [form, setForm] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
  });

  useEffect(() => {
    console.log(userData);
  });

  const updateForm = (prop, key) => (e) => {
    validation(key);
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const editDataInDB = (content, key) => {
    validation(key);

    if (Array.isArray(key)) {
      key.map((str, index) => {
        console.log('str');
        console.log(str);
      });
      key.map((str, index) => {
        userData[str] = content[index];
      });
    }
    if (typeof key === 'string') userData[key] = content;

    if (
      userData !== null &&
      ((typeof content === 'string' && content !== '') ||
        (Array.isArray(content) && content.length !== 0))
    ) {
      updateUser({
        email: userData.email,
        firstName: userData.firstName,
        lastName: userData.lastName,
        likedTweets: userData.likedTweets,
        password: userData.password,
        role: userData.role,
        uid: userData.uid,
        username: userData.username,
      });

      if (
        Array.isArray(key) &&
        key.includes('firstName') &&
        key.includes('lastName')
      ) {
        setclickEditBtnName('false');
      }
      if (key.includes('email')) setclickEditBtnEmail('false');
      if (key.includes('password')) setclickEditBtnPassword('false');
    }
  };

  function containsNumber(str) {
    return /\d/.test(str);
  }

  const validation = (key) => {
    if (form.password.length < 8 && key === 'password') {
      document.forms['myForm']['password'].style.border = '2px solid red';
      document.getElementById('password-msg').innerHTML =
        'Not a valid password! Must be at least 8 symbols.';
    } else if (form.password.length > 8 && key === 'password') {
      document.forms['myForm']['password'].style.border = '2px solid green';
    }

    if (
      (typeof key !== 'string' &&
        (form.lastName.length < 4 || form.lastName.length > 32)) ||
      (containsNumber(form.lastName) && Array.isArray(key))
    ) {
      document.forms['myForm']['lastName'].style.border = '2px solid red';
      document.getElementById('lastName-msg').innerHTML =
        'Not a valid name! Must be at least 4 symbols- last name.';
    } else if (
      (typeof key !== 'string' &&
        form.lastName.length > 4 &&
        form.lastName.length < 32) ||
      (!containsNumber(form.lastName) && Array.isArray(key))
    ) {
      document.forms['myForm']['lastName'].style.border = '2px solid green';
    }

    if (
      (typeof key !== 'string' && form.firstName.length < 4) ||
      form.firstName.length > 32 ||
      (containsNumber(form.firstName) && Array.isArray(key))
    ) {
      document.forms['myForm']['firstName'].style.border = '2px solid red';
      document.getElementById('firstName-msg').innerHTML =
        'Not a valid name! Must be at least 4 symbols.- first name';
    } else if (
      (typeof key !== 'string' &&
        form.firstName.length > 4 &&
        form.firstName.length < 32) ||
      (!containsNumber(form.firstName) && Array.isArray(key))
    ) {
      document.forms['myForm']['firstName'].style.border = '2px solid green';
    }

    if (
      !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(form.email) &&
      key === 'email'
    ) {
      document.forms['myForm']['email'].style.border = '2px solid red';
      document.getElementById('email-msg').innerHTML = 'Not a valid email!';
    } else if (
      /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(form.email) &&
      key === 'email'
    ) {
      document.forms['myForm']['email'].style.border = '2px solid green';
    }
  };

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if (!file) return alert(`Please select a file!`);

    const picture = storageRef(storage, `images/${handle}/avatar`);

    uploadBytes(picture, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            return updateUserProfilePicture(handle, url).then(() => {
              setContext({
                user,
                userData: {
                  ...userData,
                  avatarUrl: url,
                },
              });
            });
          });
        })
        .catch(console.error);
  };

  return (
    <div className="Profile">
      <div className="left-section">
        <div className="text">Change your profile info</div>
      </div>

      <div className="right-section">
        <div className="profile-title">Health and Wellness </div>
        <div className="profile-info">
          <div className="profile-section">
            <div className="comp-left-user"> user: </div>

            <div className="comp-left"> name: </div>
            <div className="comp-left"> username: </div>
            <div className="comp-left"> email: </div>
            <div className="comp-left"> password: </div>
          </div>

          <div className="settings-section-right">
            <div className="profile-photos">
              <form onSubmit={uploadPicture}>
                <div className="btnimpUpload">
                  {userData.avatarUrl ? (
                    <>
                      <label htmlFor="files">
                        <img
                          src={userData.avatarUrl}
                          alt="profile"
                          className="imgProfile"
                          width="60"
                          height="60"
                          border="2px solid red"
                          border-radius="100px"
                          title="click to change picture"
                        />
                      </label>
                    </>
                  ) : (
                    <label htmlFor="files">
                      <Avatar
                        className="avatar-icon"
                        sx={{
                          bgcolor: '#296c6d',
                          fontSize: 14,
                          width: 80,
                          height: 80,
                        }}
                      />
                    </label>
                  )}
                  <input
                    type="file"
                    id="files"
                    name="file"
                    className="inputUpload"
                    data-buttonText="Find file"
                  ></input>
                  <button type="submit" className="btnUpload">
                    <GrUpload className="test123" />
                  </button>
                </div>
              </form>
            </div>
            {userData !== null && (
              <form action="/" name="myForm" className="Form-settings">
                <>
                  <div className="comp-right">
                    {clickEditBtnName === 'true' ? (
                      <>
                        <div className="saveButtonForEditComment">
                          <div></div>
                          <input
                            id="firstName"
                            type="text"
                            placeholder="First name..."
                            className="firstName"
                            value={form.firstName}
                            onChange={updateForm('firstName', [
                              'firstName',
                              'lastName',
                            ])}
                            background="transparent"
                          ></input>
                          <div id="firstName-msg" className="err-msg"></div>

                          <input
                            id="lastName"
                            type="text"
                            placeholder="Last name..."
                            className="lastName"
                            value={form.lastName}
                            onChange={updateForm('lastName', [
                              'firstName',
                              'lastName',
                            ])}
                            background="transparent"
                          ></input>
                          <div id="lastName-msg" className="err-msg"></div>
                        </div>
                        <div>
                          <AiOutlineSave
                            className="save-icon"
                            onClick={() =>
                              form.firstName !== '' && form.lastName !== '' ?
                                editDataInDB(
                                    [form.firstName, form.lastName],
                                    ['firstName', 'lastName']
                                ) :
                                validation(['firstName', 'lastName'])
                            }
                          />
                        </div>
                      </>
                    ) : (
                      <>
                        {userData.firstName} {userData.lastName}
                        <AiOutlineEdit
                          id="edit-icon-name"
                          className="edit-icon"
                          onClick={() => setclickEditBtnName('true')}
                        />
                      </>
                    )}
                  </div>
                  {console.log(userData.username)}
                  <div className="comp-right">{userData.username}</div>

                  <div className="comp-right">
                    {clickEditBtnEmail === 'true' ? (
                      <>
                        <div className="saveButtonForEditComment">
                          <input
                            id="email"
                            type="email"
                            placeholder="Enter email..."
                            className="email"
                            value={form.email}
                            onChange={updateForm('email', 'email')}
                            background="transparent"
                          ></input>
                          <div id="email-msg" className="err-msg"></div>
                        </div>

                        <AiOutlineSave
                          id="save-icon-email"
                          className="save-icon"
                          onClick={() =>
                            form.email !== '' ?
                              editDataInDB(form.email, 'email') :
                              validation('email')
                          }
                        />
                      </>
                    ) : (
                      <>
                        {userData.email}
                        <AiOutlineEdit
                          id="edit-icon-email"
                          className="edit-icon"
                          onClick={() => setclickEditBtnEmail('true')}
                        />
                      </>
                    )}
                  </div>

                  <div className="comp-right">
                    {clickEditBtnPassword === 'true' ? (
                      <>
                        <div className="saveButtonForEditComment">
                          <input
                            id="password"
                            type="password"
                            placeholder="Enter password..."
                            className="password"
                            value={form.password}
                            onChange={updateForm('password', 'password')}
                            background="transparent"
                          ></input>
                          <div id="password-msg" className="err-msg"></div>
                        </div>
                        <AiOutlineSave
                          id="save-icon-password"
                          className="save-icon"
                          onClick={() =>
                            editDataInDB(form.password, 'password')
                          }
                        />
                      </>
                    ) : (
                      <>
                        {userData.password}
                        <AiOutlineEdit
                          id="edit-icon-password"
                          className="edit-icon"
                          onClick={() => setclickEditBtnPassword('true')}
                        />
                      </>
                    )}
                  </div>
                </>
              </form>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
