import {useState} from 'react';
import './CreateComment.css';

// eslint-disable-next-line valid-jsdoc
/**
 *  @param {{ onSubmit: (content: string) => Promise<any>}} param0
 * @returns
 */
const CreateComment = ({onSubmit}) => {
  const [content, setContent] = useState('');
  const createComment = () => {
    onSubmit(content)
        .then(() => {
          setContent('');
        })
        .catch((e) => alert(e.message));
  };

  return (
    <>
      <div className="comment-section">
        <label>Comment:</label><br></br>
        <textarea
          value={content}
          onChange={(e) => setContent(e.target.value)}>
        </textarea><br />
      </div>
      <div className='add-comment-btn'>
        <button onClick={createComment}>Add Comment</button>
      </div>
    </>
  );
};

export default CreateComment;
