/* eslint-disable array-callback-return */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import AppContext from '../../../data/app-state';
import {useContext, useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import Comment from '../Comment/Comment';
import CreateComment from '../../CreateComment/CreateComment';
import {
  addComment,
  deleteComment,
  dislikeComment,
  fromCommentsDocument,
  getLiveComments,
  likeComment,
} from '../../../services/comments.service';
import './Comments.css';

export default function Comments({postId, commentsForPost}) {
  const navigate = useNavigate();
  const {
    user,
    userData: {username, role},
    setContext,
  } = useContext(AppContext);
  const [comments, setComments] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveComments((snapshot) => {
      setComments(fromCommentsDocument(snapshot));
    });
    return () => unsubscribe();
  }, []);

  const createComment = (content) => {
    return addComment(content, username, postId).then((post) => {});
  };

  const like = (comment) => {
    likeComment(username, comment.id).then(() => {});
  };

  const dislike = (comment) => {
    dislikeComment(username, comment.id).then(() => {});
  };

  const delComment = (id) => {
    deleteComment(id)
        .then(() => {})
        .catch(console.error);
  };

  const logout = () => {
    setContext({user: null, userData: null});

    navigate('/home');
  };

  return (
    <> {user ?
        <>
          {commentsForPost === undefined || comments === undefined ?
              <div className='no-comments'>No comments to show</div> :
              <div className='generated-comments'>
                {comments.map((comment) => {
                  if (comment.postId === postId) {
                    return (<div className="each-comment" key={comment.id}>
                      <Comment comment={comment} deleteComment={delComment} like={like} dislike={dislike} />
                    </div>);
                  }
                })}
              </div>}
          {role === 3 ? <div className='no-comments'>You are blocked and cannot write comments!</div> :
            <div className='CreateComment'><CreateComment onSubmit={createComment} /> </div> }
        </> : logout()}
    </>
  );
}
