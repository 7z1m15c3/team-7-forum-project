import {useState, useContext} from 'react';
import './Login.css';
import {loginUser} from '../../services/auth.service';
import {getUserData} from '../../services/users.service';
import AppContext from '../../data/app-state';
import {useNavigate} from 'react-router-dom';
import {MdLogin} from 'react-icons/md';

const Login = () => {
  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const {setContext} = useContext(AppContext);
  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const login = (e) => {
    loginUser(form.email, form.password)
        .then(async (u) => {
          return await getUserData(u.user.uid).then((snapshot) => {
            if (snapshot.exists()) {
              setContext({
                user: u.user,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]],
              });
              console.log('EXISTS');

              navigate('/home');
            }
          });
        })
        .catch(console.error);
  };

  return (
    <div className="Login">
      <img src={require('./1.jpg')} alt="login-img"></img>

      <div className="user-log-in">
        <div className="title-form">
          <h2>Log in</h2>

          <div>
            <MdLogin className="profile-icon" />
          </div>
        </div>
        <div className="Form-login">
          <div className="section">
            <label htmlFor="email">Email: </label>
            <input
              type="email"
              placeholder="Enter email..."
              className="email"
              value={form.email}
              onChange={updateForm('email')}
            ></input>
          </div>

          <div className="section">
            <label htmlFor="password">Password: </label>
            <input
              type="password"
              placeholder="Enter password..."
              className="password"
              value={form.password}
              onChange={updateForm('password')}
            ></input>
          </div>
          <div className="login-btn">
            <button onClick={login}>Login</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
