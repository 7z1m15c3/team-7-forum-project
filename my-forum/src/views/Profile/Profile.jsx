/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import './Profile.css';
import {Avatar} from '@mui/material';
import {useContext, useEffect} from 'react';
import AppContext from '../../data/app-state';

export default function Profile() {
  const {userData} = useContext(AppContext);
  useEffect(() => {
    console.log(userData);
  });

  return (
    <div className="Profile">
      <div className="left-section">
        <div className="text">Review your profile info</div>
      </div>

      <div className="right-section">
        <div className="profile-title">Health and Wellness </div>
        <div className="profile-info">
          <div className="profile-section">
            <div className="comp-left-user"> user: </div>
            <div className="comp-left"> name: </div>
            <div className="comp-left"> username: </div>
            <div className="comp-left"> email: </div>
          </div>

          <div className="profile-section-right">
            {userData !== null &&
              (userData.avatarUrl !== undefined ? (
                <img
                  src={userData.avatarUrl}
                  alt="profile"
                  className="imgProfile"
                />
              ) : (
                <Avatar
                  className="avatar-icon"
                  sx={{
                    bgcolor: '#f4976c',
                    fontSize: 14,
                    width: 80,
                    height: 80,
                  }}/>
              ))}
            {userData !== null && (
              <>
                <div className="comp-right">
                  {userData.firstName} {userData.lastName}{' '}
                </div>
                <div className="comp-right">{userData.username}</div>
                <div className="comp-right">{userData.email} </div>
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
