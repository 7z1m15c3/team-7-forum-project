/* eslint-disable require-jsdoc */
/* eslint-disable array-callback-return */
/* eslint-disable max-len */
// eslint-disable-next-line max-len
import React, {useContext, useEffect, useState} from 'react';
import {useNavigate, useSearchParams} from 'react-router-dom';
import './OpenPost.css';
import {Avatar} from '@mui/material';
import Comments from '../Comments/Comments/Comments';
import {getAllCommentsByPostId} from '../../services/comments.service';
import {
  deletePost,
  getPostById,
  updatePost,
} from '../../services/posts.services';
import {MdDeleteOutline} from 'react-icons/md';
import AppContext from '../../data/app-state';
import {AiOutlineSave} from 'react-icons/ai';
import {getUserByUsername} from '../../services/users.service';

const OpenPost = () => {
  const navigate = useNavigate();
  const [searchParam] = useSearchParams();

  const postId = searchParam.get('postId');
  const edit = searchParam.get('edit');
  const [post, setPost] = useState();
  const [commentsForPost, setCommentsForPost] = useState();
  const {user, userData} = useContext(AppContext);
  const [form, setForm] = useState({contentForm: '', tagsForm: ''});
  const [userByUserName, setUserByUserName] = useState();

  useEffect(() => {
    (async () => {
      try {
        setPost(await getPostById(postId));
        setCommentsForPost(await getAllCommentsByPostId(postId));
      } catch (e) {
        console.log(e.message);
      }
    })();
    setForm({contentForm: post?.content});
    setForm({tagsForm: post?.tags});
  }, [postId]);

  useEffect(() => {
    (async () => {
      try {
        setUserByUserName(await (await getUserByUsername(post?.author)).val());
      } catch (e) {
        console.log(e.message);
      }
    })();
  }, [post]);

  const delPost = (id) => {
    deletePost(id)
        .then(() => {})
        .catch(console.error);
  };

  const postEvent = (e) => {
    e.preventDefault();
    editPost();
  };

  const updateForm = (prop) => (e) => {
    // console.log(/[^a-zа-я0-9#_]/.test(e.target.value));

    // console.log(/[^a-zа-я0-9#_]/.test(e.target.value));
    // if (prop === 'tagsForm' && /[^a-zа-я0-9#_]/.test(e.target.value)) {
    //   e.target.value = e.target.value
    //       .split('')
    //       .splice(0, e.target.value.length - 1)
    //       .join('');

    // console.log(e.target.value);
    // alert('you can write only number,letterand #');
    setForm({...form, [prop]: e.target.value});
  };

  const editPost = () => {
    const tagsArr = [];
    if (form.tagsForm === '' || form.contentForm === '') {
      return alert('you can\'t save all post or tags');
    }
    if (form?.tagsForm !== undefined) {
      const array = Array.from(form?.tagsForm);
      let indexA = 0;
      let tag;

      if (form?.tagsForm[0] !== '#') array.unshift('#');

      array.map((char, index) => {
        if (index === array.length - 1) {
          tag = array.slice(indexA, index + 1).join('');
        }
        if ((char === '#' && index !== 0) || index === array.length - 1) {
          if (index !== array.length - 1) {
            tag = array.slice(indexA, index).join('');
          }
          tagsArr.push(tag);
          indexA = index;
        }
      });
    }

    if (post !== undefined) {
      if (form.tagsForm === undefined && form.contentForm !== undefined) {
        updatePost(postId, post.title, form?.contentForm, post.tags, post.author);
      }
      if (form.tagsForm !== undefined && form.contentForm === undefined) {
        updatePost(postId, post.title, post.content, tagsArr, post.author);
      }
      if (form.tagsForm !== undefined && form.contentForm !== undefined) {
        updatePost(postId, post.title, form?.contentForm, tagsArr, post.author);
      }
    }
    navigate(`/all-posts`);
  };

  function isInputTags(evt) {
    const ch = String.fromCharCode(evt.which);
    if (!/^[0-9a-zа-я#]+$/.test(ch)) {
      evt.preventDefault();
    }
  }

  if (
    post === undefined &&
    commentsForPost === undefined &&
    (userByUserName === undefined || userByUserName === null)) {
    <div></div>;
  } else {
    return (
      <div className="posts">
        <div className="post-top">
          <div className="post-title">
            <h2>{post.title}</h2>
          </div>
          <div className="authorAndDate">
            <div className="post-author">author: @{post?.author} </div>
            <div className="post-date">
              date: {new Date(post.createdOn).toLocaleDateString()}
            </div>
          </div>
        </div>

        <div className="post-bottom">
          <hr className="second-hr" />
          <div className="postWithFirstLetter">
            <div className="auth">
              {userByUserName !== undefined &&
              userByUserName !== null &&
              userByUserName?.avatarUrl !== undefined ? (
                <img
                  src={userByUserName.avatarUrl}
                  alt="profile"
                  className="imgProfile"/>
              ) : (
                <Avatar
                  className="avatar-icon"
                  sx={{
                    bgcolor: '#f4976c',
                    fontSize: 34,
                    width: 80,
                    height: 80,
                  }}
                >
                  {post?.author.slice(0, 1)}
                </Avatar>
              )}
            </div>

            {edit !== null ? (
              <div className="postText">
                <div className="content">
                  <textarea
                    type="text"
                    className="editTextareaContent"
                    onChange={updateForm('contentForm')}
                    defaultValue={post.content}/>
                </div>
                <div className="blockTags-DeleteIcon">
                  <div id="tags">
                    <textarea
                      type="text"
                      className="editTextareaTags"
                      onChange={updateForm('tagsForm')}
                      defaultValue={post.tags}
                      onKeyPress={(e) => isInputTags(e)}
                    />
                  </div>
                  <div className="savebtn">
                    <AiOutlineSave className="save-icon" onClick={postEvent} />
                  </div>
                </div>
              </div>
            ) : (
              <div className="postText">
                <div className="content">{post.content}</div>
                <div className="blockTags-DeleteIcon">
                  <div id="tag">{post.tags}</div>
                  {user && userData?.username === post?.author && (
                    <MdDeleteOutline
                      className="delete-icon"
                      onClick={() =>
                        (() => {
                          delPost(post.id);
                          navigate('/all-posts');
                        })()
                      }/>
                  )}
                </div>
              </div>
            )}
          </div>
        </div>

        <div className='all-comments'>
          <Comments postId={post.id} commentsForPost={commentsForPost} />
        </div>
      </div>
    );
  }
};
export default OpenPost;
