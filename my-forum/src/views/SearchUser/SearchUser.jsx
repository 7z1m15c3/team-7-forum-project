/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable require-jsdoc */
import './SearchUser.css';
import {useContext, useEffect, useState} from 'react';
import {getAllUsers, updateUserRole} from '../../services/users.service';
import {VscSearch} from 'react-icons/vsc';
import {useSearchParams} from 'react-router-dom';
import AppContext from '../../data/app-state';
import {Avatar} from '@mui/material';
import ReactPaginate from 'react-paginate';
import {ImBlocked} from 'react-icons/im';
import {userRole} from '../../common/user-role.js';
import {useNavigate} from 'react-router-dom';
import {RiUserAddLine} from 'react-icons/ri';
import {getUserByUsername} from '../../services/users.service';
import {convertCompilerOptionsFromJson} from 'typescript';

const PER_PAGE = 6;

export default function SearchUser() {
  const [users, setUsers] = useState([]);
  const {user, userData, setContext} = useContext(AppContext);
  const [searchField, setSearchField] = useState('');
  const [currentPage, setCurrentPage] = useState(0);

  useEffect(() => {
    (async () => {
      setUsers(await getAllUsers());
    })();
  }, []);

  const handleChange = (e) => {
    setSearchField(e.target.value);
  };

  const handleBlock = async (username) => {
    const currUser = await getUserByUsername(username);
    currUser.val().role === 3 ?
      updateUserRole(currUser.val().username, 1) :
      updateUserRole(currUser.val().username, 3);
    document.location.reload();
  };

  const deleteSearchText = () => {
    const searchField = document.getElementById('search-field');
    searchField.value = '';
  };

  const filteredUsers = users.filter((user) => {
    return (
      user.username.toLowerCase().includes(searchField.toLowerCase()) ||
      user.email.toLowerCase().includes(searchField.toLowerCase()) ||
      user.firstName.toLowerCase().includes(searchField.toLowerCase()) ||
      user.lastName.toLowerCase().includes(searchField.toLowerCase())
    );
  });

  const handlePageClick = ({selected: selectedPage}) => {
    setCurrentPage(selectedPage);
    window.scrollTo(0, 0);
  };

  const offset = currentPage * PER_PAGE;

  const usersView = (array) =>
    array.slice(offset, offset + PER_PAGE).map((eachUser, index) => (
      <div className="each-user" key={eachUser.id}>
        <div className="user-index">{index + 1}</div>
        <div className="avatar">
          {eachUser.avatarUrl !== undefined ? (
            <>
              <div className="photoSearchUser">
                <img
                  src={eachUser.avatarUrl}
                  alt="profile"
                  className="imgProfile"
                />
              </div>
              <div className="each-user-username">{eachUser.username}</div>
            </>
          ) : (
            <>
              <div className="photoSearchUser">
                <Avatar
                  className="avatar-icon"
                  sx={{
                    bgcolor: '#f4976c',
                    fontSize: 34,
                    width: 80,
                    height: 80,
                  }}/>
              </div>
              <div className="each-user-username">{eachUser.username}</div>
            </>
          )}
        </div>
        <div className="user-email">{eachUser.email}</div>
        <div className="each-user-role">
          {eachUser.role === 1 ?
            'Basic' :
            eachUser.role === 2 ?
            'Admin' :
            'Blocked'}
        </div>
        <div className="user-text-names">
          {eachUser.firstName} {eachUser.lastName}
        </div>

        <button onClick={() => handleBlock(eachUser.username)}>
          {eachUser.role === 3 ? (
            <RiUserAddLine className="unblocked" />
          ) : (
            <ImBlocked />
          )}
        </button>
      </div>
    ));

  return (
    <div className="Search-user">
      <div className="search-user-header">
        <div className="search-user-title">
          <h2>Search user: </h2>
        </div>
        <div className="search-user-section">
          <input
            className="search-user-bar"
            type="search"
            name="search-form"
            placeholder="Search here..."
            id="search-user"
            onChange={handleChange}/>
          <VscSearch onClick={deleteSearchText} className="search-button" />
        </div>
      </div>

      <div className="body-all-users">
        <div className="user-head-menu">
          <div>#</div>
          <div>user</div>
          <div>email</div>
          <div>role</div>
          <div>name</div>
        </div>

        <div>
          <hr />
        </div>
        {user && users.length !== 0 ? (
          <>
            {usersView(filteredUsers)}
            <ReactPaginate
              breakLabel="..."
              nextLabel="next >"
              onPageChange={handlePageClick}
              pageRangeDisplayed={PER_PAGE}
              pageCount={Math.ceil(filteredUsers.length / PER_PAGE)}
              previousLabel="< previous"
              renderOnZeroPageCount={null}
              containerClassName={'pagination'}
              previousLinkClassName={'pagination-link'}
              nextLinkClassName={'pagination-link'}
              disabledClassName={'pagination-disabled'}
              activeClassName={'pagination-active'}/>
          </>
        ) : (
          'No users to show!'
        )}
      </div>
    </div>
  );
}
