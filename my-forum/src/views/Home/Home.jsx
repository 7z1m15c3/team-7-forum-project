/* eslint-disable react-hooks/exhaustive-deps */
import React, {useContext, useEffect, useState} from 'react';
import './Home.css';
import {useNavigate} from 'react-router-dom';
import {BsChatLeftText} from 'react-icons/bs';

import {BiUser} from 'react-icons/bi';
import {
  fromPostsDocument,
  getLivePosts,
} from '../../services/posts.services';
import Post from '../Post/Post';
import {MdListAlt} from 'react-icons/md';
import AppContext from '../../data/app-state';
import {getAllUsers} from '../../services/users.service';

const Home = () => {
  const [posts, setPosts] = useState([]);
  const {userData} = useContext(AppContext);
  const [users, setUsers] = useState([]);

  const navigate = useNavigate();

  const routeChange = () => {
    const path = `/newpost`;
    if (userData.role === 3) {
      document.getElementById('blocked-msg').innerHTML =
    'You are blocked and cannot write new posts!';
    } else {
      navigate(path);
    }
  };

  useEffect(() => {
    const unsubscribe = getLivePosts((snapshot) => {
      console.log('changes detected');
      console.log(userData);

      setPosts(fromPostsDocument(snapshot));
    });
    (async () => {
      setUsers(await getAllUsers());
    })();

    return () => unsubscribe();
  }, []);

  return (
    <>
      <div className="Home">
        <div className="new-post-btn">
          <button className="btnNewPosts" onClick={routeChange}>
            <div>New post</div>
            <div><BsChatLeftText /></div>
          </button>

          <div id='blocked-msg'></div>

          <div className="all-count-section">
            <div className="icons">
              <BiUser />
              {users.length}
            </div>
            <div className="icons">
              <MdListAlt />
              {posts.length}
            </div>
          </div>
        </div>

        <div className="all-post-home">
          {posts.map((post, key) => {
            if (key < 10) {
              return (
                <div className="each-post" key={post.id}>
                  <Post
                    post={post}
                  />
                </div>
              );
            }
            // eslint-disable-next-line array-callback-return
            return;
          })}
        </div>
      </div>
    </>
  );
};

export default Home;
