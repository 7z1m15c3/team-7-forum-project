/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import AppContext from '../../data/app-state';
import {useNavigate} from 'react-router-dom';
import './AllPosts.css';
import ReactPaginate from 'react-paginate';
import {fromPostsDocument, getLivePosts} from '../../services/posts.services';
import Post from '../../views/Post/Post';
import {MdListAlt} from 'react-icons/md';
import {useContext, useEffect, useState} from 'react';

const PER_PAGE = 5;

export default function AllPosts() {
  const navigate = useNavigate();

  const [posts, setPosts] = useState([]);
  const {user, userData, setContext} = useContext(AppContext);
  const [currentPage, setCurrentPage] = useState(0);

  const [sortParam, setSortParam] = useState('*');
  const [filterParam, setFilterParam] = useState('*');

  const [sortArr, setSortArr] = useState(posts);
  const [filterArr, setFilterArr] = useState(sortArr);

  useEffect(() => {
    const unsubscribe = getLivePosts((snapshot) => {
      console.log('changes detected');
      setPosts(fromPostsDocument(snapshot));
      setSortArr(fromPostsDocument(snapshot));
      setFilterArr(fromPostsDocument(snapshot));
    });

    setSortParam();

    return () => unsubscribe();
  }, []);

  const handlePageClick = ({selected: selectedPage}) => {
    setCurrentPage(selectedPage);
    window.scrollTo(0, 0);
  };

  const logout = () => {
    setContext({user: null, userData: null});

    navigate('/home');
  };

  const offset = currentPage * PER_PAGE;

  const currentPageData = (filteredArr) =>
    filteredArr.slice(offset, offset + PER_PAGE).map((post) => (
      <div className="each-post" key={post.id}>
        <Post post={post} />
      </div>
    ));

  const sortArrFunc = (sortBy, arrOfPosts) => {
    console.log('in sort func');
    console.log(sortBy);
    switch (sortBy) {
      case 'last-added': {
        const arrCopy = JSON.parse(JSON.stringify(arrOfPosts));

        return arrCopy.sort(function(a, b) {
          console.log('lastAdded');
          console.log('arrOfPosts', arrCopy);
          return new Date(b.createdOn) - new Date(a.createdOn);
        });
      }
      case 'first-added': {
        const arrCopy = JSON.parse(JSON.stringify(arrOfPosts));

        return arrCopy.sort(function(a, b) {
          console.log('first-added');
          console.log('arrOfPosts', arrCopy);
          return new Date(a.createdOn) - new Date(b.createdOn);
        });
      }
      case 'most-liked': {
        const arrCopy = JSON.parse(JSON.stringify(arrOfPosts));

        return arrCopy.sort(function(a, b) {
          console.log('most-liked');
          console.log('arrOfPosts', arrCopy);
          return b.likedBy.length - a.likedBy.length;
        });
      }
      case 'less-liked': {
        const arrCopy = JSON.parse(JSON.stringify(arrOfPosts));

        return arrCopy.sort(function(a, b) {
          console.log('less-liked');
          console.log('arrOfPosts', arrCopy);
          return a.likedBy.length - b.likedBy.length;
        });
      }
      case 'by-author': {
        const arrCopy = JSON.parse(JSON.stringify(arrOfPosts));

        return arrCopy.sort(function(a, b) {
          const authorA = a.author.toUpperCase();
          const authorB = b.author.toUpperCase();
          if (authorA < authorB) {
            return -1;
          }
          if (authorA > authorB) {
            return 1;
          }
          return 0;
        });
      }

      default:
        return arrOfPosts;
    }
  };

  const filterArrFunc = (filterBy, sortedArray) => {
    console.log('filter');
    switch (filterBy) {
      case '*': {
        return sortedArray;
      }

      case 'liked': {
        const arrCopy = JSON.parse(JSON.stringify(sortedArray));

        return arrCopy.filter((post) => post.likedBy.length !== 0);
      }

      case 'your-posts': {
        const arrCopy = JSON.parse(JSON.stringify(sortedArray));

        return arrCopy.filter((post) => post.author === userData.username);
      }

      default: return 0;
    }
  };

  return (
    <div className="All-posts">
      <div className="head-all-post">
        <div className="title-all-post">
          <h2>All posts:</h2>
        </div>

        <div className="all-post-count">
          <MdListAlt className="list-post-icon" />

          <div className="post-number">{filterArr.length}</div>
        </div>
      </div>
      {user ? (
        <div className="body-all-post">
          <div className="head-menu">
            <div className="sort">
              <div className="label">
                <label>Sort by:</label>
              </div>

              <select
                aria-controls="sort-menu"
                data-filter="true"
                className="select"
                onChange={(e) => {
                  setSortParam(e.target.value);
                  setSortArr(sortArrFunc(e.target.value, posts));
                  setFilterArr(filterArrFunc(filterParam, sortArrFunc(e.target.value, posts)));
                }}>
                <option value="*">All</option>
                <option value="last-added">Last added</option>
                <option value="first-added">First added</option>
                <option value="most-liked">Most liked</option>
                <option value="less-liked">Less liked</option>
                <option value="by-author">By author A - Z</option>
              </select>
            </div>

            <div className="filter">
              <div className="label">
                <label>Filter by:</label>
              </div>

              <select
                aria-controls="filter-menu"
                data-filter="true"
                className="select"
                onChange={(e) => {
                  setFilterParam(e.target.value);
                  setFilterArr(filterArrFunc(e.target.value, sortArrFunc(sortParam, posts)));
                }}>
                <option value="*">None</option>
                <option value="liked">Liked</option>
                <option value="your-posts">Your post</option>
              </select>
            </div>
          </div>
          <div>
            <hr />
            <hr className="second-hr" />
          </div>

          <div className="all-posts-body">
            {filterArr.length === 0 ? (
              <p>No posts to show.</p>
            ) : (
              <>
                {currentPageData(filterArr)}
                <ReactPaginate
                  breakLabel="..."
                  nextLabel="next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={PER_PAGE}
                  pageCount={Math.ceil(filterArr.length / PER_PAGE)}
                  previousLabel="< previous"
                  renderOnZeroPageCount={null}
                  containerClassName={'pagination'}
                  previousLinkClassName={'pagination-link'}
                  nextLinkClassName={'pagination-link'}
                  disabledClassName={'pagination-disabled'}
                  activeClassName={'pagination-active'}
                />
              </>
            )}
          </div>
        </div>
      ) : (
        logout()
      )}
    </div>
  );
}
