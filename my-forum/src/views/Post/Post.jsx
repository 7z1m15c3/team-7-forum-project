/* eslint-disable no-unused-vars */
/* eslint-disable func-call-spacing */
/* eslint-disable no-unexpected-multiline */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import React, {useContext, useEffect, useState} from 'react';
import AppContext from '../../data/app-state';
import {Avatar} from '@mui/material';
import {BsChatRightTextFill} from 'react-icons/bs';
import {NavLink} from 'react-router-dom';
import {AiOutlineEdit} from 'react-icons/ai';
import {FcLikePlaceholder} from 'react-icons/fc';
import {FcLike} from 'react-icons/fc';
import {
  deletePost,
  dislikePost,
  likePost,
} from '../../services/posts.services';
import {MdDeleteOutline} from 'react-icons/md';
import {getAllUsers} from '../../services/users.service';

/**
 * @param {{ post: { id: string, content: string, author: string, date: Date, likedBy: string[] } } } post
 */

const Post = ({post}) => {
  console.log('in Post');

  const {user, userData} = useContext(AppContext);
  const [users, setUsers] = useState([]);
  const isPostLiked = user ? post.likedBy.includes(userData.username) : false;
  let bool = false;

  const handleLikeButtonClick = () =>
    isPostLiked ? dislike(post) : like(post);

  useEffect(() => {
    (async () => {
      setUsers(await getAllUsers());
    })();
  }, []);

  const like = (post) => {
    likePost(userData.username, post.id).then(() => { });
  };

  const dislike = (post) => {
    dislikePost(userData.username, post.id).then(() => { });
  };

  const delPost = (id) => {
    deletePost(id)
        .then(() => { })
        .catch(console.error);
  };

  const postUser = users.map((user, i) => {
    if (user?.id === post?.author && user.avatarUrl !== undefined) {
      bool = true;
      return (<img src={user.avatarUrl} alt="profile" className='imgProfile' />);
    } else if (users.length - 1 === i && bool === false) {
      return (<Avatar
        className="avatar-icon"
        sx={{bgcolor: '#f4976c', fontSize: 34, width: 80, height: 80}}
      ></Avatar>);
    }
    if (i === users.length - 1) {
      bool = false;
    }
  });

  return (
    <div className="post">
      <div className="avatar">{postUser}</div>

      <div className="post-content">
        <div className="content">
          <div className="title">
            <div>
              <NavLink
                to={`/openpost?postId=${post.id}`}
                className="title-content">
                {post?.title}
              </NavLink>
              <div className="authorData">
                <label className="labelAuthor">author:</label> {post?.author}
              </div>
            </div>
            <div className="post-icon-home">
              <NavLink to={`/openpost?postId=${post.id}`}>
                <BsChatRightTextFill className="comment-icon" />
              </NavLink>
              {user !== null &&
                (post.author === userData.username || userData.role === 2) ?
                (
                  <>
                    <NavLink to={`/openpost?postId=${post.id}&edit`}>
                      <AiOutlineEdit className="edit-icon" />
                    </NavLink>
                    <MdDeleteOutline
                      className="delete-icon"
                      onClick={() => delPost(post.id)}/>
                  </>
                ) : null}
            </div>
          </div>
        </div>

        <div>
          <hr />
          <hr className="second-hr" />
        </div>

        <div className="right-section-post">
          <div className="post-like">
            <div className="post-like-nmb">
              {user !== null ? (
                <button onClick={handleLikeButtonClick}>
                  {isPostLiked ? (
                    <FcLike className="post-likes" />
                  ) : (
                    <FcLikePlaceholder className="post-likes" />
                  )}
                </button>
              ) : <div></div>}

              <span>{post.likedBy.length} likes</span>
            </div>
          </div>

          <div className="authorDateTime">
            {new Date(post.createdOn).toLocaleDateString()}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Post;
