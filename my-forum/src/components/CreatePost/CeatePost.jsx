/* eslint-disable require-jsdoc */
/* eslint-disable max-len */
import './CreatePost.css';
import {Avatar} from '@mui/material';
import AppContext from '../../data/app-state';
import {useContext, useState} from 'react';

/**
 * @param {{ onSubmit: (content: string) => Promise<any>}} param0
 * @returns
 */

const CreatePost = ({onSubmit}) => {
  const {userData} = useContext(AppContext);
  const [form, setForm] = useState({
    content: '',
    tags: '',
    title: '',
  });

  const createPost = () => {
    const array = Array.from(form?.tags);
    const tagsArr = [];
    let indexA = 0;
    let tag;
    if (form?.tags[0] !== '#') array.unshift('#');

    array.map((char, index) => {
      if (index === array.length - 1) {
        tag = array.slice(indexA, index + 1).join('');
      }
      if ((char === '#' && index !== 0) || index === array.length - 1) {
        if (index !== array.length - 1) {
          tag = array.slice(indexA, index).join('');
        }
        tagsArr.push(tag);
        indexA = index;
      }
    });
    if (form?.title !== '' && form?.content !== '' && form.tags!=='' && !(form.content.length < 32 ||
      form.content.length > 8192)) {
      const titleM = form.title.toLowerCase();

      const titleFinal = titleM[0].toUpperCase() + titleM.slice(1);
      onSubmit(titleFinal, form.content, tagsArr)
          .then((e) => {
            alert('Post created!');
            // (e).target.value='';
          })
          .catch((e) => alert(e.message));
    } else alert('you have not filled in all the fields or you have added fewer characters than you need ');
  };

  function isInputTags(evt) {
    const ch = String.fromCharCode(evt.which);
    if (!/^[0-9a-zа-я#]+$/.test(ch)) {
      evt.preventDefault();
    }
  }

  function containsNumberLetters(str, key) {
    if (key === 'content') /[^A-Za-zА-Яа-я0-9_]/.test(str); // .-,
    else if (key === 'tags') /[^a-zа-я0-9#_]/.test(str);
    else if (key === 'title')/[^A-Za-zА-Яа-я0-9_]/.test(str);
  }

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const validation = () => {
    if (
      (form.title.length < 4 && form.title.length > 32) ||
      containsNumberLetters(form.title, 'title')
    ) {
      document.forms['myForm']['title'].style.border = '2px solid red';
      document.getElementById('title-msg').innerHTML = 'Not a valid title! Must be between 4 and 32 symbols (numbers and letters).';
      alert('title - Must be between 4 and 32 symbols');
    }
    if (
      form.content.length < 32 ||
      form.content.length > 8192 ||
      containsNumberLetters(form.content, 'content')
    ) {
      document.forms['myForm']['content'].style.border = '2px solid red';
      document.getElementById('content-msg').innerHTML = 'Not a valid content! Must be between 32 symbols and 8192 symbols (numbers,letters, -.,).';
      alert('content - Must be between 32 symbols and 8192 symbols');
    }
    if (containsNumberLetters(form.tags, 'tags')) {
      document.forms['myForm']['tags'].style.border = '2px solid red';
      document.getElementById('tags-msg').innerHTML =
        'Not a valid tags! Must contain only lowercase letters, numbers and #.';
    }
  };

  const post = (e) => {
    e.preventDefault();
    validation();
    createPost();
  };

  return (
    <div className="CreatePost">
      <div className="header-create-post">
        <div className="title-create-post">
          <h2>Create a post</h2>
        </div>

        <div className="create-post-user">
          <div className="profile-info">
            {userData !== null &&
              (userData.avatarUrl !== undefined ? (
                <div className="avatar">
                  <img
                    src={userData.avatarUrl}
                    alt="profile"
                    className="imgProfile"
                  />
                </div>
              ) : (
                <div className="avatar">
                  <Avatar
                    className="avatar-icon"
                    sx={{
                      bgcolor: '#b4dfe5',
                      fontSize: 34,
                      width: 80,
                      height: 80,
                    }}/>
                </div>
              ))}
            <div className="user">username: {userData.username}</div>
            <div className="email">email: {userData.email}</div>
          </div>
        </div>
      </div>

      <div className="body-view">
        <form action="/" name="myForm" className="Form-post">
          <div className="create-post-body">
            <div className="create-post">
              <label>Title: </label>
              <textarea
                id="title"
                type="text"
                placeholder="Enter title..."
                className="title-text"
                value={form.title}
                onChange={updateForm('title')}
              ></textarea>
              <div id="title-msg" className="err-msg"></div>
              <br />
            </div>

            <div className="create-post">
              <label>Content: </label>
              <textarea
                id="content"
                type="text"
                placeholder="Enter content..."
                className="content-text"
                value={form.content}
                onChange={updateForm('content')}
              ></textarea>
              <div id="content-msg" className="err-msg"></div>

              <br />
            </div>

            <div className="create-post">
              <label>Tags: </label>
              <textarea
                id="tags"
                type="text"
                placeholder="Enter tags..."
                className="tags-text"
                value={form.tags}
                onChange={updateForm('tags')}
                onKeyPress={(e) => isInputTags(e, e.target.value.length)}
              ></textarea>
              You can use only #, letter and numbers
              <div id="tags-msg" className="err-msg"></div> <br />
            </div>

            <button type="submit" onClick={post}>Post</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CreatePost;
